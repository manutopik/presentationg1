# Presentation with [Slidev](https://github.com/slidevjs/slidev)!

Online : https://presentation-g1.netlify.app

To change content :

- clone this repository
- `pnpm install`
- `pnpm dev`
- visit http://localhost:3030

Edit the [slides.md](./slides.md) to see the changes.

Learn more about Slidev on [documentations](https://sli.dev/).
