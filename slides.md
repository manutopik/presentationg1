---
theme: default
class: text-left text-5xl
highlighter: shiki
lineNumbers: false
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
drawings:
  persist: false
title: Monnaie libre
---

<div class="flex items-center">
  <div class="">

  # Monnaie libre

  ### Repenser la création monétaire... et l'expérimenter !
  
  </div>

  <img src="/logo.svg" class="w-40 ml-20" />
</div>

<div class="mt-30 text-gray-700">

#### Emmanuel Salomon
##### @ManUtopiK
##### 2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8

</div>

---
layout: center
---

# La monnaie libre, c'est quoi ?

- 📝 **Un théorie** - la TRM : Théorie Relative de la Monnaie
- ⚗ **Une expérience** - la Ğ1, la "G" une, la june !


<div v-click>

<br />

_Utilisé par des économistes pour financer la transition écologique (libre de dette)._

</div>

---
layout: center
---

# 1000€, c'est beaucoup ?

1. **Relatif à l'individu** - Jeff Bezos (Amazon) 🆚 les 99%
2. **Relatif à l'objet** - 🚗 ou 🚙
3. **Relatif au coût de la vie** - 🥖 et ⛽

<div v-click class="mt-10 bg-blue-100 p-3 px-6 rounded">

Selon la monnaie libre :

$\cfrac{15\ 390\ milliards\ d’euros}{340\ millions\ de\ citoyens}=44\ 118\ €\ /\ citoyen$

Inflation : +24,80 € / jour / citoyen

<div class="text-sm pt-4">Sources : https://www.creationmonetaire.info</div>

</div>

---
layout: center
---

# L'argent, d'où ça vient ?

<div class="grid grid-cols-2 gap-10">
  <div>

  - ❌ N'est pas issue du fruit de notre travail  
  - 🧂 Tout peut être monnaie d'échange  
  - ~~🥇 L'étalon Or~~ -> Promesses

  </div>
  <div>

  1. Banque centrale 🏦
  2. Crédits + réserves obligatioires 1%
  3. Le marché 📈

  </div>
</div>

<div v-click class="mt-10">

<div class="text-2xl text-center bg-red-100 p-3 px-6 rounded mb-10">
😱 Les économistes ne sont même pas d'accords !
</div>

###### Plus d'infos :

[Chaine youtube Heu?reka](https://www.youtube.com/channel/UC7sXGI8p8PvKosLWagkK9wQ)

</div>

---
layout: center
---

# La dette

Intérêts manquants

🏦 > 100 000 € > 🏠

- ~~**Principal**~~ : 100 000 €
- **Intérêts** : 50 000 € ?

<br>

> « Les crédits font les dépôts »
— Gouverneur de la banque centrale Suisse

<div v-click class="mt-10 bg-red-100 p-3 px-6 rounded mb-10">

<div class="text-2xl text-center ">
💥 Fuite en avant !
</div>

</div>

---
layout: two-cols
---

# La théorie relative de la monnaie

Écrite en 2010 par Stéphane Laborde

### 4 libertés

1. La liberté de choix de son système monétaire
2. La liberté d’utiliser les ressources
3. La liberté d’estimation et de production de toute valeur économique
4. La liberté d’échanger, comptabiliser, afficher ses prix “dans la monnaie”

<br>

#### ↘ **Co-création monétaire** par tous les êtres humains = _DIVIDENDE UNIVERSEL_

::right::

<img src="/renouvellement.png" />

---

# Equitable <small>entre les individus, dans le temps et l'espace</small>

$DU=c\ *\ \cfrac{Masse\ monétaire}{Nombre\ de\ membres}$ <span class="mx-10"></span>c = ~ 10% pour 80 ans

<div class="grid grid-cols-2 items-center mt-10">
<img src="/ajout10.png" />
<img src="/redistribution.png" />
</div>

<div v-click class="mt-10 bg-green-100 p-3 px-6 rounded mb-10 text-center text-2xl">
🌞 Vers une économie de flux plutôt qu'une économie de stock ?
</div>

---
layout: image-right
image: arrosage.jpg
---

# Ça change quoi ?

- Durable et juste, à long terme
- Développe les biens et les services de l'économie réelle
- Simple et transparent
- Pas de dette pertétuelle
- Limite la spéculation
- Pas d'inflation, ni déflation

<br>
<br>

### 📏 Unité de mesure invariant !

---
layout: iframe-right
url: https://carte.monnaie-libre.fr?ui=false
---
# La june Ğ1

C'était possible, alors ils l'ont fait !

- Créée il y a 5 ans, un 8 mars
- ~4 500 membres
- +17 000 portefeuilles

**Application de la TRM avec le numérique :**
<br>

#### ⛓ Blockchain + 🕸 Toile de confiance

---

# Cryptage : hash

Blockchain et NFT

<ShaInput />

---
layout: image-right
image: message-ecole.png
---

# Chiffrage

|**a**|**b**|**c**|**d**|**e**|**…**|
| --- | --- | --- | --- | --- | --- |
|x|k|f|a|v|…|

---

# Chiffrage

<img src="/chiffrement.png" class="w-full px-50" />

1. Communication sécurisé boout en bout
2. Authentification = chèque !

---
layout: two-cols
---

# Toile de confiance

But : un DU = un être vivant

- 5 certifications 🎖
- 2 mois max
- à renouveler tous les 2 ans
- réglès de distance 📏

C'est lent, mais ça créé du lien !

::right::
<Tdc class="h-130" />

---
layout: center
class: text-center
---

<img src="/bonsang.jpg" class="w-50 mb-10" />

# En savoir plus

[https://monnaie-libre.fr](https://monnaie-libre.fr)

## 🎉